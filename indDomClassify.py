from sklearn import svm
from pandas import DataFrame
import numpy
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB

rows = []
index = []

a = ["this is sample text oil", " this is another sample text tech"]
b = ["12345", "67890"]

rows.append({'text': a[0], 'class': 'OIL'})
index.append(b[0])
rows.append({'text': a[1], 'class': 'TECH'})
index.append(b[1])

data_frame = DataFrame(rows, index=index)

print(type(data_frame))


count_vectorizer = CountVectorizer()
counts = count_vectorizer.fit_transform(data_frame['text'].values)
targets = data_frame['class'].values

#print(counts)
# Below is Naive Bayes Classifier
#classifier = MultinomialNB()
#classifier.fit(counts, targets)

examples = ['Free Viagra call tech today!', "I'm going to attend the Linux users tech oil group tomorrow."]
example_counts = count_vectorizer.transform(examples)
#predictions = classifier.predict(example_counts)
#print(predictions) # [1, 0]


# SVM classification
clf = svm.SVC()
clf.fit(counts, targets)  

print(clf.predict(example_counts))
